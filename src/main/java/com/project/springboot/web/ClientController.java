package com.project.springboot.web;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.project.springboot.model.Client;
import com.project.springboot.service.ClientService;



@Controller
public class ClientController {
	
	@Autowired
	private ClientService clientService;
	
	
	@RequestMapping("/getAll")
	public String getAll(Model model) {
		List<Client> clients = clientService.getClients();
		model.addAttribute("clients", clients);
		return "clients";
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Optional<Client> getOne(Long id){
		return clientService.getOne(id);
	}
	
	@PostMapping("/addNew")
	public String addNew(@ModelAttribute("client") Client client){
		clientService.saveClient(client);
		return "redirect:/getAll";
	}
//	
//	@PostMapping
//	public String registerUserAccount( UserRegistrationDto registrationDto) {
//		userService.save(registrationDto);
//		return "redirect:/registration?success";
//	}
	
	@RequestMapping(value="/update", method = {RequestMethod.PUT, RequestMethod.GET})
	public String update(Client client) {
		clientService.updateClient(client);
		return "redirect:/getAll";
	}
	
	@RequestMapping(value="/delete", method = {RequestMethod.DELETE, RequestMethod.GET})
	public String delete(Long id) {
		clientService.deleteClient(id);
		return "redirect:/getAll";
	}
	
	
	
}
