package com.project.springboot.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;


@Controller
public class OAuthController {
	
	@Autowired
	OAuth2AuthorizedClientService authclientService;
	
	

		@RequestMapping("/oauth2LoginSuccess")
		public String getOauth2LoginInfo(Model model,@AuthenticationPrincipal OAuth2AuthenticationToken authenticationToken) {
			// fetching the client details and user details 
			
			System.out.println("O auth login");
			
//			System.out.println(authenticationToken.getAuthorizedClientRegistrationId()); // client name like facebook, google etc.
//			System.out.println(authenticationToken.getName()); // facebook/google userId
//				
//			//		1.Fetching User Info
//			OAuth2User user = authenticationToken.getPrincipal(); // When you login with OAuth it gives you OAuth2User else UserDetails
//			System.out.println("userId: "+user.getName()); // returns the userId of facebook something like 12312312313212
//			// getAttributes map Contains User details like name, email etc// print the whole map for more details
//			System.out.println("Email:"+ user.getAttributes().get("email"));
//	        
//			//2. Just in case if you want to obtain User's auth token value, refresh token, expiry date etc you can use below snippet
//		       OAuth2AuthorizedClient client = authclientService
//		         .loadAuthorizedClient(authenticationToken.getAuthorizedClientRegistrationId(), 
//					  authenticationToken.getName());
//	              System.out.println("Token Value"+ client.getAccessToken().getTokenValue()); 
//			
//		   //3. Now you have full control on users data.You can eitehr see if user is not present in Database then store it and 
//		   // send welcome email for the first time 
//		    model.addAttribute("name", user.getAttribute("name"));
		    return "clients";
		}
	    
	    @RequestMapping("/formLoginSuccess")
		public String getFormLoginInfo(Model model, @AuthenticationPrincipal Authentication authentication) {
	        // In form-based login flow you get UserDetails as principal while in Oauth based flow you get Oauth2User 
	    	
	    	Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
	    	
	    	String username = loggedInUser.getName();
			
//			model.addAttribute("name", userDetails.getUsername());
			return "/";

		}
}
