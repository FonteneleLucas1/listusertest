package com.project.springboot.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.project.springboot.model.Client;
import com.project.springboot.repository.ClientRepository;




@Service
public class ClientService {
	
	@Autowired
	private ClientRepository clientRepository;
	
	public Client saveClient(Client client) {
		return clientRepository.save(client);
	}
	
	public List<Client> getClients() {
		return clientRepository.findAll();
	}
	
	public Optional<Client> getOne(Long id) {
		return clientRepository.findById(id);
	}
	
	public String deleteClient(Long id) {
		clientRepository.deleteById(id);
		return "Deleted by id: " + id;
	}
	
	public Client clientById(Long id) {
		Client existingClient = clientRepository.findById(id).orElse(null);
		return existingClient;
	}
	
	public Client updateClient(Client client) {
		Client existingClient = clientRepository.findById(client.getId()).orElse(null);
		existingClient.setName(client.getName());
		return clientRepository.save(existingClient);
	}
	
}
