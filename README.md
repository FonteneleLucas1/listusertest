# Projeto Teste CodeFiction: 
## SPRING BOOT - H2 - GOOGLE SOCIAL - MAVEN - JUNIT - CRUD


### Deploy local:
  ## Gerando arquivo .jar
    Executar Maven build (goals): build compile install - irá gerar o arquivo executável .jar na pasta target;

  ## Executando Deploy com Docker
    Executar o comando: docker-compose up

  ## Visualizar saída localmente:
    localhost:8080

  ## Visualizar saída web - Amazon EC2:
    http://ec2-18-222-231-85.us-east-2.compute.amazonaws.com:8080/



### Utilização:
    Criar usuário ou entrar com login social GOOGLE;
    Criar user com sessão ativa (crud);
    Os dados de login via FORM e SOCIAL GOOGLE são compartilhados;
    Logout encerra a sessão;



### Requisitos : done & to do

Criar uma API simples de users. Deve permitir essas funções: listar users,
deletar users, fazer login e fazer logout. Utilizar Spring BOOT. **done**

● Login social (Google já tá bom). Pode usar Spring Social, ou outra ferramenta
a vontade; **done** 

● Enviar link de ativação de conta (só deve poder acessar a API após ativação,
antes deve mostrar mensagem que precisa ativar). **to do** (não feito por questão de tempo para implementação)

● DB pode ser a vontade (sugestão h2, postgres ou mongo). **done** (h2)

● Adicionar swagger, devemos poder fazer todo o flow pelo swagger. Dica:
documentar todos os campos. **to do** 

● Não esquecer dos unit tests. **to do** (testes apenas iníciados)

● Criar dockerfile e docker-compose.yaml pra fazer o deploy local como
container do docker. **done**

● Entregar o link do repositório em modo privado (preferência gitlab) com
readme.MD com instruções de como rodar localmente e resumo técnico. **done**

● Escrever o Readme.md Instruções do flow de login. **done**

● Bônus: criar CI/CD (sugestão gitlab CI/CD ou github actions) com 3 stages: compile,
tests e dockerize (gerar container do docker). Bônus: Deploy em algum cloud
provider (preferência AWS, pode ser no EC2 mesmo, ou Azure ou Heroku) **done** (Amazon ec2) 
http://ec2-18-222-231-85.us-east-2.compute.amazonaws.com:8080/






###### by: Lucas Fontenele: fontenelelucas8@gmail.com / (86) 994666972


